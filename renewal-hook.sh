#!/bin/bash

# Besure to create certificate directories on container host
#sudo mkdir -p /etc/noVNCcert
#sudo mkdir -p /etc/XRDPcerts

# Also make certbot renewal hook directory on host. This path is refernece in the certbot command as seen in the example at the end.
#sudo mkdir -p /etc/certbothook

# Hosting server name
SERVER_HOSTNAME="vcm-39013.vm.duke.edu"

# Copy XRDP certificates to /etc/XRDPcerts directory on container host
sudo cp /etc/letsencrypt/live/$SERVER_HOSTNAME/cert.pem /etc/XRDPcerts/cert.pem
sudo cp /etc/letsencrypt/live/$SERVER_HOSTNAME/privkey.pem /etc/XRDPcerts/key.pem

# Concatenate and copy the noVNC private key, certificate and certificate chain to /etc/noVNCcerts on container host
sudo cat /etc/letsencrypt/live/$SERVER_HOSTNAME/privkey.pem /etc/letsencrypt/live/$SERVER_HOSTNAME/fullchain.pem > /etc/noVNCcert/self.pem

# Docker run example with mounting certificate directories on host server to their corresponding /noVNC/ and /etc/xrdp/ directories in # the container. Renewed certificates should automatically be placed in the container directories when the certbot renewal happens 
# using #this renewal hook script. 

#LISTENPORT=6080
#sudo docker run \
#   -d -t -p $LISTENPORT:$LISTENPORT \
#   -p 3389:3389 \
#   -e LISTENPORT=6080 \
#   -e VNCPASS='secret' \
#   -e UBUNTUPASS='secret' \
#   -v /etc/noVNCcert:/noVNC/ \
#   -v /etc/XRDPcerts:/etc/xrdp/ \
#   -h your-host.oit.duke.edu docker-novnc-baseimage
# https://your-host.vm.duke.edu:6080/index.html?encrypt=1&autoconnect=1&password=secret&port=6080

# Certbot command - The deploy hook directory should reflect where you put the script on the host. Make it executable!
# sudo certbot certonly --standalone -d vcm-39013.vm.duke.edu --server https://locksmith.oit.duke.edu/acme/v2/directory --agree-tos -m ae57@duke.edu --deploy-hook /etc/certbothook/renewal-hook.sh 
